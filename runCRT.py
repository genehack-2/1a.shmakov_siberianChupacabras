import sys
from subprocess import call

if len(sys.argv) != 4:
    print "Usage: python runCRT listOfOrgName blastDB crtPATH"
    print "Apply CRT to magor genomes in blastDB"

listOfOrgName = sys.argv[1]
blastDB = sys.argv[2]
crtPATH = sys.argv[3]

with open(listOfOrgName, "r") as listOfOrg:
    for line in listOfOrg:
        if line[0] == "#":
            continue
        quant = line.split()
        if quant[-1].strip() == "minor":
            continue

        NCnum = quant[1]
        NTgi = quant[-2]
        fnaFileName = "{0}.fasta".format(NCnum)
        crtResFileName = "{0}.crt".format(NCnum)
        call("blastdbcmd -db {0} -entry {1} > {2}"\
			.format(blastDB, NTgi, fnaFileName), shell=True)
        call("java -cp {0} crt {1} {2}"\
			.format(crtPATH, fnaFileName, crtResFileName), shell=True)


# Search for family in protein
if [ $# -ne 4 ]; then
    echo "Usage: ./findDomen.sh proteinsFile hmmFile resultFile litterFile"
    exit 1
fi


hmmsearch -o $4 --domtblout _temp_$3 $2 $1
cat _temp_$3 | awk ' $0!~"#" {print $1 "\t" $4 "\t" $5 "\t" $7}' > $3
rm -f _temp_$3
rm -f litter.txt

import sys
import os
from Bio import SeqIO
from subprocess import call
import argparse

def readBFasta(fastaFileName):
    with open(fastaFileName, "r") as fastaFile:
        sequences = SeqIO.parse(fastaFile, "fasta")
        return {seq : [] for seq in sequences}

def writeBFasta(sequences, fastaFileName, filtName, saveDescr):
    with open(fastaFileName, "w") as fastaFile:
        for seq, domens in sequences.items():
            if saveDescr:
                desc =  seq.description.split(None, 1)
                if len(desc) > 1:
                    name = seq.name + " " + desc[1]
                else:
                    name = seq.name
            else:
                name = seq.name

            if domens:
                name = name + " {0}:{1}".format(filtName, ":" + "_".join(domens))

            fastaFile.write(">{0}\n{1}\n".format(name, seq.seq))

parser = argparse.ArgumentParser(\
    description = "Scipt for filtering operons by occuring must-be activities in them")

parser.add_argument("inFileDir", type = str, help = "Dir with operons in fasta format")
parser.add_argument("outFileDir", type = str, help = "Dir with results")
parser.add_argument("hmmFiltersFile", type = str, help = "Filters list")
parser.add_argument("-saveDescr", help = "Save seq description or not", nargs = '?',\
                    const = False, default = True)

argv = parser.parse_args()

inFileDir = argv.inFileDir
outFileDirPref = argv.outFileDir
hmmFiltersFileName = argv.hmmFiltersFile
saveDescr = argv.saveDescr

litterFileName = "litter.txt"

filters = {}
filt_order = []
with open(hmmFiltersFileName, "r") as hmmFiltersFile:
    for line in hmmFiltersFile:
        if line[0] == "#":
            continue
        name, path = line.split()
        filters[name] = path
        filt_order.append(name)

dirNum = 0
for filtName in filt_order:
    print "Start filter {0}".format(filtName)
    hmmFilterDir = filters[filtName]
    dirNum += 1
    outFileDir = "{0}filt_{1}/".format(outFileDirPref, dirNum)
    os.mkdir(outFileDir)
    inFileNames = ["{0}{1}".format(inFileDir, x) for x in os.listdir(inFileDir)\
               if "." in x and x.split(".")[-1] == "fasta"]

    hmmFileNames = ["{0}{1}".format(hmmFilterDir, x) for x in os.listdir(hmmFilterDir)\
                if "." in x and x.split(".")[-1] == "hmm"]


    for inFileName in inFileNames:
        print "\tProcess file {0}".format(inFileName)
        sequences = readBFasta(inFileName)
        tempFileName = "temp.txt"
        with open(tempFileName, "w") as tempFile:
            pass

        for hmmFileName in hmmFileNames:
            call("bash findDomen.sh {0} {1} {2} {3}".\
                 format(inFileName, hmmFileName, tempFileName, litterFileName),\
                shell = True)
            family = os.path.basename(hmmFileName).split(".")[0]
            with open(tempFileName, "r") as tempFile:
                for line in tempFile:
                    for seq, domens in sequences.items():
                        if seq.name in line:
                            domens.append(family)
        os.remove(tempFileName)

        resFileName = "{0}{1}".format(outFileDir, os.path.basename(inFileName))
        for domens in sequences.values():
            if len(domens) > 0:
                writeBFasta(sequences, resFileName, filtName, saveDescr)
                print "\t\tpassed filter"
                break
        else:
            print "\t\tdidnt pass"
    inFileDir = outFileDir


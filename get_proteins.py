import crt # homemade module for parsing CRT out
import pickle
import sys
import os
from subprocess import call
import argparse

def save_object(obj, filename):
    with open(filename, 'wb') as output:
            pickle.dump(obj, output, pickle.HIGHEST_PROTOCOL)

def load_object(filename):
    with open(filename, 'rb') as input:
        return pickle.load(input)

parser = argparse.ArgumentParser(\
    description = "Script to get proteins near repeat, proposed by CRT")
parser.add_argument("crtOutDir", type = str, help = "outCrtDir")
parser.add_argument("dbPath", type = str, help = "Path to standalone blast database")
parser.add_argument("orfListFile", type = str,\
    help = "File with ORFs for all genomes in blast standalone database")
parser.add_argument("-operonDeterm", type = str,\
    help = "How to determine possible CRISPR operon, by dist between proteins or by their num only",\
    choices = ["maxNum", "dist"], default = "maxNum")
parser.add_argument("-dist", type = int, default = 100,\
    help = "dist between proteins in operon if you choose <dist> option in operonDeterm")
parser.add_argument("-maxProtNum", type = int, default = 4,\
    help = "number of proteins if you choose if you choose <protNum> option in operonDeterm")

argv = parser.parse_args()

crtOutDir = argv.crtOutDir
databasePath = argv.dbPath
orfListFileName = argv.orfListFileName
maxOperonDist = argv.maxOperonDist
maxProtNum = argv.maxProtNum
operonDeterm = argv.operonDeterm # [maxNum dist] by distance or by num of proteins

genomes = {}

pickleFileName = "{0}_dict.pkl".format(orfListFileName.split()[0])

if pickleFileName in os.listdir("./"):
    print "Dict for file exist"
    #load_object(pickleFileName) ## too long
else:
    print "Read orf file"
    with open(orfListFileName, "r") as orfListFile:
        for line in orfListFile:
            parse = line.strip().split()
            pID = parse[0]
            stend = parse[1].split("..")
            start = int(stend[0])
            end = int(stend[1])
            strand = parse[2]
            NCnum = parse[4]
            if NCnum not in genomes:
                genomes[NCnum] = []
            genomes[NCnum].append((pID, start, end, strand,))
    #save_object(genomes, pickleFileName)
    print "End"


crtOutFilesNames = ["{0}/{1}".format(crtOutDir, x) for x in os.listdir(crtOutDir) if len(x.split(".")) > 1\
                    and x.split(".")[-1] == "crt"]

print "Search for proteins"
for crtOutFileName in crtOutFilesNames:
    crtOUT = crt.crtParse(crtOutFileName)
    NCnum = os.path.basename(crtOutFileName).split(".")[-2]
    print "Search for {0}".format(NCnum)
    for crNum, rep in enumerate(crtOUT.predicted_crisprs, start = 1):
        print "Search proteins for {0} predicted CRISPR".format(crNum)
        findPlusOperon = False
        findMinusOperon = False
        proteins = genomes[NCnum]
        #plus strand case
        for i, protein in enumerate(proteins):
            if not findPlusOperon and protein[1] >= rep.start and protein[3] == "+":
                #start searching for crispOperon
                if operonDeterm == "maxNum":
                    plusOperon = [proteins[i - 1]]

                    j = i - 2
                    curNum = 1
                    while (j >= 0 and curNum < maxProtNum):
                        if proteins[j][3] == "+":
                            plusOperon.append(proteins[j])
                            curNum += 1
                        j -= 1

                    findPlusOperon = True
                    continue

                plusOperon = [proteins[i - 1]]
                j = i - 2
                while (j >= 0 and proteins[j][3] == "-"):
                    j -= 1

                operonEnd = int(proteins[i - 1][1])
                while (j >= 0 and operonEnd - proteins[j][2] <= maxOperonDist):
                    plusOperon.append(proteins[j])
                    operonEnd = proteins[j][1]
                    j -= 1
                    while (j >= 0 and proteins[j][3] == "-"):
                        j -= 1
                findPlusOperon = True

            if not findMinusOperon and protein[1] >= rep.end and protein[3] == "-":
                if operonDeterm == "maxNum":

                    minusOperon = [proteins[i]]

                    j = i + 1
                    curNum = 1
                    while (j < len(proteins) and curNum < maxProtNum):
                        if proteins[j][3] == "-":
                            minusOperon.append(proteins[j])
                            curNum += 1
                        j += 1
                    findMinusOperon = True
                    break

                minusOperon = [proteins[i]]
                operonEnd = rep.end
                j = i + 1
                while (j < len(proteins) and proteins[j][3] == "+"):
                    j += 1

                while (j < len(proteins) and (proteins[j][1] - operonEnd) > maxOperonDist):
                    minusOperon.append(proteins[j])
                    operonEnd = proteins[j][2]
                    j += 1
                    while (j < len(proteins) and proteins[j][3] == "+"):
                        j += 1
                findMinusOperon = True
                break

        outFileName = "{0}_cr_{1}_prot_plus.fasta".format(NCnum, crNum)

        query = "echo '" + "\n".join([protein[0] for protein in plusOperon]) + "' "
        call("{0} | blastdbcmd -db {1} -entry_batch - > {2}".\
             format(query, databasePath, outFileName), shell = True)

        outFileName = "{0}_cr_{1}_prot_minus.fasta".format(NCnum, crNum)
        query = "echo '" + "\n".join([protein[0] for protein in minusOperon]) + "' "
        call("{0} | blastdbcmd -db {1} -entry_batch - > {2}".\
             format(query, databasePath, outFileName), shell = True)



import sys
import re
from Bio import Seq

class CrispRepOcc:
    def __init__(self, position, repeat, spacer):
        self.position = position
        self.repeat = repeat
        self.spacer = spacer


class CrispRep:
    def __init__(self, start, end, occurences, averageRepLen, averageSpacerLen):
        self.start = start
        self.end = end
        self.occurences = occurences
        self.averageRepLen = averageRepLen
        self.averageSpacerLen = averageSpacerLen


class CRT:
    def __init__(self, organism, genomLen, predictedCrispRep):
        self.organism = organism
        self.genomLen = genomLen
        self.predicted_crisprs = predictedCrispRep

def crtParse(inFileName):
    with open(inFileName, "r") as inFile:
        head1 = inFile.readline()
        if "ORGANISM:" != head1[:9]:
            sys.stderr.write("Error, inFile is not in crt format\n")
            exit(1)
        organism = head1[10:].strip()
        head2 = inFile.readline()
        if "Bases:" != head2[:6]:
            sys.stderr.write("Error, inFile is not in crt format\n")
            exit(1)

        genomLen = int(head2[7:].strip())

        predictedCrispRep = []

        for line in inFile:
            if line.strip() == "":
                pass

            curLine = re.match(\
                "CRISPR\s+[0-9]\s+Range:\s+(?P<start>[0-9]+)\s+-\s+(?P<end>[0-9]+)",
                               line)
            if curLine:
                start = int(curLine.group("start"))
                end = int(curLine.group("end"))
                occurences = []

            curLine = re.match(\
                     "(?P<position>[0-9]+)\s+(?P<repeat>[ATGC]+)\s(?P<spacer>[ATGC]+)?",
                                   line)
            if curLine:
                position = int(curLine.group("position"))
                repeat = curLine.group("repeat")
                spacer = curLine.group("spacer")
                occurences.append(CrispRepOcc(position, repeat, spacer))

            curLine = re.match(\
                "Repeats:\s[0-9]+\sAverage\sLength:\s(?P<arl>[0-9]+)\s+Average\sLength:\s(?P<asl>[0-9]+)",
                    line)
            if curLine:
                averageRepLen = int(curLine.group("arl"))
                averageSpacerLen = int(curLine.group("asl"))
                predictedCrispRep.append(CrispRep(start, end, occurences,\
                                            averageSpacerLen, averageRepLen))

        return CRT(organism, genomLen, predictedCrispRep)




if __name__ == "__main__":
    for rep in crtParse(sys.argv[1]).predicted_crisprs:
        for occ in rep.occurences:
            print occ.spacer


